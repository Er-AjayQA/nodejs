const express = require("express");
const router = express.Router();
const usersController = require("../controllers/usersController");

router.post("/sign-up", usersController.userSignup);
router.post("/login", usersController.userLogin);
router.get("/logout", usersController.userLogout);
router.get("/admin", usersController.adminPage);

module.exports = router;
