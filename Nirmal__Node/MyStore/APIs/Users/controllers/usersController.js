const db = require("../../../indexFiles/modelsIndex");
const jwt = require("jsonwebtoken");
const Users = db.tbl_users;
const secreteKey = "It is a secrete key";

module.exports.userSignup = async (req, res) => {
  try {
    const { userName, emailId, password, confirmPassword } = req.body;
    const userExist = await Users.findOne({ where: { emailId } });
    const comparePwd = password === confirmPassword;

    if (!userExist) {
      if (comparePwd) {
        const user = await Users.create({
          userName,
          emailId,
          password,
          confirmPassword,
        });
        res.send("User Registered Successfully!!");
      } else {
        res.send("Password and Confirm Password not matching!!");
      }
    } else {
      res.send("EmailId Already Exist!!");
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports.userLogin = async (req, res) => {
  try {
    const { emailId, password } = req.body;
    const userExist = await Users.findOne({ where: { emailId } });

    if (userExist) {
      if (userExist.password === password) {
        // req.session.isLoggedIn = "true";
        const token = jwt.sign(
          { isLoggedIn: "true", userName: userExist.userName },
          secreteKey
        );
        req.session.token = token;
        res.send("Login Successfully!!");
      } else {
        res.send("Invalid Credentials!!");
      }
    } else {
      res.send("User Don't Exist!!");
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports.adminPage = async (req, res) => {
  try {
    const token = req.session.token;
    jwt.verify(token, secreteKey, (err, decoded) => {
      if (!err) {
        res.send(token);
      } else {
        res.send("User Not Authorized!!");
      }
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports.userLogout = (req, res) => {
  try {
    // req.session.isLoggedIn = "false";
    req.session.destroy();
    res.send("User Logged-out Successfully!!");
  } catch (error) {
    console.log(error);
  }
};
