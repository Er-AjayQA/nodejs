const express = require("express");
const cookie = require("cookie-parser");
const config = require("./config/db.config");
const session = require("express-session");
const MysqlSession = require("express-mysql-session")(session);
const JWT = require("jsonwebtoken");
const app = express();
const port = 8000;
const secreteKey = "It is a secrete key";

//========== Section Configuration ==========//
const options = {
  host: config.HOST,
  user: config.USER,
  password: config.PASSWORD,
  database: config.DB,
  connectionLimit: 10,
  createDatabaseTable: true,
};
const sessionStore = new MysqlSession(options);

app.use(
  session({
    secret: secreteKey,
    resave: false,
    saveUninitialized: false,
    store: sessionStore,
  })
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookie());

//========== Home Route ==========//
app.get("/", (req, res) => {
  try {
    res.send("Welcome to MyStore API!!");
  } catch (error) {
    console.log("=========== ERROR ===========", error);
  }
});

app.get("/jwt", (req, res) => {
  try {
    const token = JWT.sign({ isLoggedIn: "true" }, secreteKey);
    res.cookie("token", token);
    res.send(token);
  } catch (error) {
    console.log(error);
  }
});

app.get("/verifyJwt", (req, res) => {
  try {
    const token = req.cookies.token;
    const validate = JWT.verify(token, secreteKey);
    console.log(validate);
    res.send(token);
  } catch (error) {
    console.log(error);
  }
});

//========== DATABASE ==========//
const db = require("./indexFiles/modelsIndex");
const Role = db.role;

//========== DB Sync ==========//
// db.sequelize
//   .sync({ alter: true })
//   .then(() => {
//     console.log("Synced db success...");
//   })
//   .catch((err) => {
//     console.log("Failed to sync db...", err.message);
//   });

//============= Listening TO The Server =============//
app.listen(port, (err) => {
  if (!err) {
    console.log(`Server running at port no : ${port}`);
  } else {
    console.log("==== Server Listening Error ====", err);
  }
});

//================= Router ==================//
const userRouter = require("./APIs/Users/routers/userRouter");
const cookieParser = require("cookie-parser");
app.use(userRouter);
