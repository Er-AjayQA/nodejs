// ========== IMPORTS and CONFIGS ========== //

const express = require("express");
require("dotenv").config();
const path = require("path");
const db = require("./indexFiles/modelsIndex");
const multer = require("multer");

// ========== COMMON CONFIG ========== //
const app = express();
const Role = db.role;
const port = process.env.PORT || 8000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ========== VIEW ENGINE CONFIG ========== //

const staticPath = path.join("public", "css");
app.use(express.static(staticPath));
app.set("view engine", "pug");
app.set("views", "views");

// ========== MULTER CONFIG ========== //

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./images");
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    cb(null, file.originalname);
  },
});

app.use(
  multer({
    storage,
    fileFilter: (req, file, cb) => {
      const fileTypes = ["image/jpeg", "image/png"];
      if (fileTypes.includes(file.mimetype)) {
        cb(null, true);
      } else {
        return cb(new Error("Only PNG and JPEG is allowed!"), false);
      }
    },
  }).single("document")
);

// ========== HOME ROUTES ========== //

app.get("/", (req, res) => {
  try {
    res.render("home/homePage");
  } catch (error) {
    console.log(error);
  }
});

// ========== DB SYNC ========== //

// db.sequelize
//   .sync({ alter: true })
//   .then(() => {
//     console.log("Synced db success...");
//   })
//   .catch((err) => {
//     console.log("Failed to sync db...", err.message);
//   });

// ==== Drop All Tables ==== //
// db.sequelize.drop();

// ========== LISTENING SERVER ========== //

app.listen(port, (err) => {
  if (!err) {
    console.log(`Server running at port : ${port}`);
  } else {
    console.log(err);
  }
});

// ========== ROUTES ========== //

const homeRouter = require("./APIs/home/router/homeRouter");
const multerRouter = require("./APIs/multer/router/multerRouter");
const studentRouter = require("./APIs/students/router/studentRouter");
app.use("/api/v1", homeRouter);
app.use("/api/v1", multerRouter);
app.use("/api/v1", studentRouter);
