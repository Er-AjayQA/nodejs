module.exports.getHomePage = async (req, res) => {
  try {
    res.render("home/homePage");
  } catch (error) {
    console.log(error);
  }
};

module.exports.getMulterPage = async (req, res) => {
  try {
    res.render("multer/multerPage");
  } catch (error) {
    console.log(error);
  }
};
