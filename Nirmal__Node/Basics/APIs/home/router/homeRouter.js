const express = require("express");
const router = express.Router();
const homeController = require("../controller/homeController");

router.get("/home", homeController.getHomePage);
router.get("/multer", homeController.getMulterPage);

module.exports = router;
