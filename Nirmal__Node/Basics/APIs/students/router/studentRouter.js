const express = require("express");
const router = express();
const studentController = require("../controllers/studentController");

// ========== Create Student ========== //
router.post("/createStudent", studentController.createStudent);

// ========== Create Student ========== //
router.put("/updateStudent/:student_id", studentController.updateStudent);

// ========== Delete Student ========== //
router.patch("/deleteStudent/:student_id", studentController.deleteStudent);

// ========== Allocate Scores ========== //
router.post("/allocateScores/:student_id", studentController.allocateScores);

// ========== Update Scores ========== //
router.post("/updateScores/:student_id", studentController.updateScores);

// ========== Delete Scores ========== //
router.delete("/deleteScores/:student_id", studentController.deleteScores);

// ========== Create School Details ========== //
router.post("/createSchoolDetails", studentController.createSchoolDetail);

// ========== Update School Details ========== //
router.post("/updateSchoolDetails/:id", studentController.updateSchoolDetail);

// ========== Delete School Details ========== //
router.patch("/deleteSchoolDetails/:id", studentController.deleteSchoolDetail);

// ========== Get All Schools List ========== //
router.get("/getAllSchoolsList", studentController.getAllSchoolsList);

// ========== Get All Students Details ========== //
router.get("/getStudentAllDetails", studentController.getStudentAllDetails);

// ========== Get Single Student Details ========== //
router.get(
  "/getSingleStudentDetail/:student_id",
  studentController.getSingleStudentDetail
);

// ========== Get Students Scores Only ========== //
router.get("/getStudentScores/:student_id", studentController.getStudentScores);

// ========== Get Students Group By School ========== //
router.get(
  "/getStudentsGroupBySchool",
  studentController.getStudentsGroupBySchool
);
module.exports = router;
