module.exports = (sequelize, Sequelize) => {
  const tbl_scores = sequelize.define(
    "tbl_scores",
    {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        allowNull: false,
        primaryKey: true,
      },
      score: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      student_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "tbl_students",
          key: "id",
        },
      },
      isDeleted: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
        validate: {
          isIn: {
            args: [[0, 1]],
            msg: "isDeleted must be either 0 or 1",
          },
        },
      },
    }
    // {
    //   hooks: {
    //     afterSync: async (options) => {
    //       if (options.force || options.alter) {
    //         await sequelize.query(
    //           "ALTER TABLE tbl_marks AUTO_INCREMENT = 1001;"
    //         );
    //       }
    //     },
    //   },
    // }
  );

  return tbl_scores;
};
