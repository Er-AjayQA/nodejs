module.exports = (sequelize, Sequelize) => {
  const tbl_schools = sequelize.define("tbl_schools", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    school_name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    ranking: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    city: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    state: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    pinCode: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    country: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    isDeleted: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        isIn: {
          args: [[0, 1]],
          msg: "isDeleted must be either 0 or 1",
        },
      },
    },
  });
  return tbl_schools;
};
