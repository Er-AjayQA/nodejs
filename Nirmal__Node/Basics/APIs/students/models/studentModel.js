module.exports = (sequelize, Sequelize) => {
  const tbl_students = sequelize.define("tbl_students", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      unique: true,
      allowNull: false,
      primaryKey: true,
    },
    roll_no: {
      type: Sequelize.INTEGER,
      unique: true,
      allowNull: false,
      validate: {
        len: {
          args: [5, 5], // Assuming the length of roll number is 5.
          msg: "Roll number must be of 5 digit",
        },
      },
    },
    s_name: {
      type: Sequelize.STRING,
      allowNull: false,
      // get() {
      //   const rawValue = this.getDataValue("s_name");
      //   return rawValue ? rawValue.toUpperCase() : null;
      // },
    },
    email_id: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: { msg: "Must be a valid email address" },
      },
    },
    isDeleted: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        isIn: {
          args: [[0, 1]],
          msg: "isDeleted must be either 0 or 1",
        },
      },
    },
  });
  return tbl_students;
};
