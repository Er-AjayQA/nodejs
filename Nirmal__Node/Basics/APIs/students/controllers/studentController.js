const { Model, where } = require("sequelize");
const db = require("../../../indexFiles/modelsIndex");
const Students = db.tbl_students;
const Scores = db.tbl_scores;
const Schools = db.tbl_schools;

// ========== Create Student ========== //
module.exports.createStudent = async (req, res) => {
  try {
    const { roll_no, s_name, email_id, school_id } = req.body;
    const studentExist = await Students.findOne({ where: { email_id } });

    if (studentExist) {
      res.send({
        status: 400,
        data: {
          message: "Student Already Exist!!",
        },
      });
    } else {
      const student = await Students.create({
        roll_no,
        s_name,
        email_id,
        school_id,
      });
      console.log(student.s_name);
      res.send({
        status: 201,
        data: {
          message: "Student Created Successfully!!",
          students: student,
        },
      });
    }
  } catch (error) {
    // console.log(error);
    if (error.name === "SequelizeValidationError") {
      const errorMessages = error.errors.map((err) => err.message);
      res.status(400).send({
        status: 400,
        error: errorMessages,
      });
    } else {
      console.log(error);
      res.status(500).send({
        status: 500,
        error: "An unexpected error occurred.",
      });
    }
  }
};

// ========== Update Student ========== //
module.exports.updateStudent = async (req, res) => {
  try {
    const id = req.params.student_id;

    const studentExist = await Students.findByPk(id);

    if (studentExist) {
      const { roll_no, s_name, email_id, school_id } = req.body;
      await Students.update(
        {
          roll_no,
          s_name,
          email_id,
          school_id,
        },
        {
          where: { id },
          returning: true, // Return the updated record(s)
        }
      );
      const updatedStudent = await Students.findByPk(id);
      res.send({
        status: 201,
        data: {
          message: "Student Details Updated Successfully!!",
          students: updatedStudent,
        },
      });
    } else {
      res.send({
        status: 400,
        data: {
          message: "Student not found!!",
        },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Delete Student ========== //
module.exports.deleteStudent = async (req, res) => {
  try {
    const id = req.params.student_id;

    const studentDetail = await Students.findByPk(id);

    if (studentDetail.isDeleted) {
      res.send({
        status: 201,
        data: { message: "Student Already Deleted!!" },
      });
    } else {
      await Students.update({ isDeleted: 1 }, { where: { id } });
      await Scores.update({ isDeleted: 1 }, { where: { student_id: id } });

      res.send({
        status: 201,
        data: { message: "Student Deleted Successfully!!" },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Allocate Scores ========== //
module.exports.allocateScores = async (req, res) => {
  try {
    const student_id = req.params.student_id;
    const { score, status } = req.body;
    const scores = await Scores.create({ score, status, student_id });
    res.send({
      status: 201,
      data: { message: "Scores Allocated Successfully!!", scores },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Update Scores ========== //
module.exports.updateScores = async (req, res) => {
  try {
    const id = req.params.student_id;
    const checkExistScore = await Scores.findOne({ where: { student_id: id } });

    if (checkExistScore) {
      const { score, status } = req.body;
      const scoresUpdated = await Scores.update(
        { score, status },
        { where: { student_id: id }, returning: true }
      );
      if (scoresUpdated) {
        const scores = await Scores.findOne({ where: { student_id: id } });
        res.send({
          status: 201,
          data: { message: "Scores Allocated Successfully!!", scores },
        });
      } else {
        res.send({
          status: 400,
          data: { message: "Error in Updating Scores!!" },
        });
      }
    } else {
      res.send({
        status: 201,
        data: { message: "Scores not allocated yet!!" },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Delete Scores ========== //
module.exports.deleteScores = async (req, res) => {
  try {
    const id = req.params.student_id;
    const deleteStudent = await Scores.destroy({ where: { student_id: id } });

    if (deleteStudent) {
      res.send({
        status: 201,
        data: { message: "Scores Deleted Successfully!!" },
      });
    } else {
      res.send({
        status: 400,
        data: { message: "Can't delete the Record!!" },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Create School Details ========== //
module.exports.createSchoolDetail = async (req, res) => {
  try {
    const { school_name, ranking, city, state, pinCode, country } = req.body;
    const schoolDetail = await Schools.create({
      school_name,
      ranking,
      city,
      state,
      pinCode,
      country,
    });
    res.send({
      status: 201,
      data: {
        message: "School Details Added Successfully!!",
        schoolDetails: schoolDetail,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Update School Details ========== //
module.exports.updateSchoolDetail = async (req, res) => {
  try {
    const id = req.params.id;
    const checkSchoolExist = await Schools.findByPk(id);

    if (checkSchoolExist) {
      const { school_name, ranking, city, state, pinCode, country } = req.body;
      const schoolUpdated = await Schools.update(
        {
          school_name,
          ranking,
          city,
          state,
          pinCode,
          country,
        },
        { where: { id } }
      );
      if (schoolUpdated) {
        const schoolDetail = await Schools.findByPk(id);
        res.send({
          status: 201,
          data: {
            message: "School Details Updated Successfully!!",
            schoolDetails: schoolDetail,
          },
        });
      } else {
        res.send({
          status: 201,
          data: {
            message: "School Details can't be updated!!",
          },
        });
      }
    } else {
      res.send({
        status: 400,
        data: {
          message: "School Details not yet added!!",
        },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Delete School Details ========== //
module.exports.deleteSchoolDetail = async (req, res) => {
  try {
    const id = req.params.id;

    await Schools.update({ isDeleted: 1 }, { where: { id } });

    res.send({
      status: 201,
      data: {
        message: "School Details Deleted Successfully!!",
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Get All Schools List ========== //
module.exports.getAllSchoolsList = async (req, res) => {
  try {
    const schoolList = await Schools.findAll({ where: { isDeleted: 0 } });
    if (schoolList) {
      res.send({
        status: 201,
        data: {
          message: "Schools List fetched successfully!!",
          records: schoolList.length,
          schoolList,
        },
      });
    } else {
      res.send({
        status: 400,
        data: {
          message: "No Schools Added Yet!!",
        },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Get All Students Details ========== //
module.exports.getStudentAllDetails = async (req, res) => {
  try {
    const query = `SELECT roll_no, s_name, email_id, score, status, school_name 
                   FROM tbl_students s
                   INNER JOIN tbl_scores m ON s.id = m.student_id
                   INNER JOIN tbl_schools d ON d.id = s.school_id
                   WHERE s.isDeleted = 0`;
    const details = await db.sequelize.query(query, {
      type: db.sequelize.QueryTypes.SELECT,
    });

    if (details.length > 0) {
      res.send({
        status: 201,
        data: {
          message: "Student's All Details",
          records: details.length,
          students: details,
        },
      });
    } else {
      res.send({
        status: 400,
        data: {
          message: "No details found",
        },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Get Single Student Details ========== //
module.exports.getSingleStudentDetail = async (req, res) => {
  try {
    const student_id = req.params.student_id;
    const studentExist = await Students.findOne({ where: { id: student_id } });

    if (studentExist) {
      // ==== Using Query ==== //
      // const query = `SELECT roll_no, s_name, email_id, score, status, school_name
      //              FROM tbl_students s
      //              INNER JOIN tbl_scores m ON s.id = m.student_id
      //              INNER JOIN tbl_schools d ON d.id = s.school_id
      //              WHERE s.id = ${student_id}`;
      // const details = await db.sequelize.query(query, {
      //   replacements: { student_id },
      //   type: db.sequelize.QueryTypes.SELECT,
      // });

      // ==== Using ORM ==== //
      const details = await Students.findByPk(student_id, {
        attributes: ["roll_no", "s_name", "email_id"],
        include: [
          {
            model: Schools,
            attributes: [
              "school_name",
              "ranking",
              "city",
              "state",
              "pinCode",
              "country",
            ],
          },
          {
            model: Scores,
            as: "student",
            attributes: ["score", "status"],
          },
        ],
      });

      res.send({
        status: 201,
        data: {
          message: "Student Details",
          student: details,
        },
      });
    } else {
      res.send({
        status: 400,
        data: {
          message: "Student not exist!!",
        },
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Get Student Scores ========== //
module.exports.getStudentScores = async (req, res) => {
  try {
    const student_id = req.params.student_id;
    const scores = await Scores.findOne({
      where: { student_id },
      include: [
        { model: Students, as: "student", attributes: ["s_name", "email_id"] },
      ],
    });
    res.send({
      status: 201,
      data: { message: "Students Score!!", scores },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};

// ========== Get Students Group By School ========== //
module.exports.getStudentsGroupBySchool = async (req, res) => {
  try {
    // ==== Order ==== //
    // const students = await Students.findAll({ order: [["id", "ASC"]] });
    // ==== Limit ==== //
    // const students = await Students.findAll({ limit: 2 });
    // ==== Offsets ==== //
    // const students = await Students.findAll({ offset: 2 });
    // ==== Count ==== //
    // const students = await Students.count({ where: { school_id: 1 } });
    // ==== Max, Min & Sum ==== //
    // const students = await Students.max("school_id");
    // const students = await Students.min("school_id");
    // const students = await Students.sum("school_id");
    // ==== Increment / Decrement ==== //
    // const students = await Students.increment({ roll_no: 10 },{ where: { id: 1 } }
    // );
    // const students = await Students.decrement({roll_no:5},{ where: { id: 1 } });
    // ==== findALl() ==== //
    // const students = await Students.findAll();
    // ==== findByPk() ==== //
    // const students = await Students.findByPk(2);
    // ==== findOne() ==== //
    const students = await Students.findOne({ where: { school_id: 2 } });
    res.send({
      status: 201,
      data: { message: "Students By School!!", students },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      error: "An unexpected error occurred.",
    });
  }
};
