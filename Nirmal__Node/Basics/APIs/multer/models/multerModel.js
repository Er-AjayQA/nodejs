module.exports = (sequelize, Sequelize) => {
  const tbl_images = sequelize.define("tbl_images", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    path: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
  });
  return tbl_images;
};
