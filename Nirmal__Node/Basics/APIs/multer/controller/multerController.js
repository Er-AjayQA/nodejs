const db = require("../../../indexFiles/modelsIndex");
const Images = db.tbl_images;

module.exports.uploadFile = async (req, res) => {
  try {
    const file = req.file;

    if (!file) {
      res.send("Choose the file first!!");
    } else {
      const imagePath = req.file.destination + "/" + req.file.filename;
      const fileSaved = await Images.create({ path: imagePath });
      res.send({
        code: 200,
        data: { message: "File saved successfully", fileSaved },
      });
    }
  } catch (error) {
    console.log(error);
  }
};
