const express = require("express");
const router = express.Router();
const multerController = require("../controller/multerController");

router.post("/uploadFile", multerController.uploadFile);

module.exports = router;
