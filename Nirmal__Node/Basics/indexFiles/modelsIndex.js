const config = require("../configFiles/db.config");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// ========== MODEL ========== //
db.tbl_users = require("../APIs/users/models/userModel")(sequelize, Sequelize);

db.tbl_images = require("../APIs/multer/models/multerModel")(
  sequelize,
  Sequelize
);

db.tbl_students = require("../APIs/students/models/studentModel")(
  sequelize,
  Sequelize
);

db.tbl_scores = require("../APIs/students/models/scoresModel")(
  sequelize,
  Sequelize
);

db.tbl_schools = require("../APIs/students/models/schoolModel")(
  sequelize,
  Sequelize
);

db.tbl_students.hasOne(db.tbl_scores, {
  as: "student",
  foreignKey: "student_id",
});
db.tbl_scores.belongsTo(db.tbl_students, {
  as: "student",
  foreignKey: "student_id",
});
db.tbl_students.belongsTo(db.tbl_schools, { foreignKey: "school_id" });
db.tbl_schools.hasMany(db.tbl_students, { foreignKey: "school_id" });

module.exports = db;
