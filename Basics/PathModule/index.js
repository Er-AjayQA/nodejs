const path = require("path");

// Extension of Path
const extension = path.extname(
  "E:MyWork/FE_Development/nodejs/ThapaStudies/PathModule/index.js"
);
console.log(extension);

// Directory name
const dirName = path.dirname(
  "E:MyWork/FE_Development/nodejs/ThapaStudies/PathModule/index.js"
);
console.log(dirName);

// BaseName
const baseName = path.basename(
  "E:MyWork/FE_Development/nodejs/ThapaStudies/PathModule/index.js"
);
console.log(baseName);

// Parse
const parse = path.parse(
  "E:MyWork/FE_Development/nodejs/ThapaStudies/PathModule/index.js"
);
console.log(parse);
console.log(parse.dir); // Gives the Directory path of the file
console.log(parse.base); // Gives the basename of the file.
console.log(parse.ext); // Gives extension of the file.
console.log(parse.name); // Gives name of the file.
console.log(parse.root); // Gives Root directory name.
