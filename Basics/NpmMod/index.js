const chalk = require("chalk");
const validate = require("validator");

console.log(chalk.blue("Hello world!"));

const url = "www.google";
const email = "ak2681993";

function formValidate(value) {
  if (value) {
    console.log("It's a valid URL!!");
  } else {
    console.log("Not a valid input!!");
  }
}

formValidate(validate.isURL(url));

const res = validate.isEmail(email);
console.log(res ? chalk.green(res) : chalk.bgRed(res));
