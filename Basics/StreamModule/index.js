// 2nd Way
// Reading from a stream
// Create a readable Stream
// Handle Stream Events => data, end and error

const fs = require("fs");
const http = require("http");

const server = http.createServer();

server.on("request", (req, res) => {
  //   // Old Way without Streaming
  //   fs.readFile("input.txt", (err, data) => {
  //     if (err) return console.error(err);
  //     res.end(data.toString());
  //   });
  // // 2nd Way of Streaming the Data
  // const rstream = fs.createReadStream("input.txt");
  // rstream.on("data", (chunkdata) => {
  //   res.write(chunkdata);
  // });
  // rstream.on("end", () => {
  //   res.end();
  // });
  // rstream.on("error", (err) => {
  //   console.log(err);
  //   res.end("File not found");
  // });
  // 3rd Way of Streaming Data using Stream Pipes()
  const rstream = fs.createReadStream("input.txt");
  rstream.pipe(res);
});

server.listen(8000, "127.0.0.1", () => {
  console.log("Listening to the server at port number: 8000");
});
