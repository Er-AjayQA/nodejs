const fs = require("fs");

const bioData = {
  name: "Ajay Kumar",
  age: 30,
  designation: "Tester",
};
// console.log(bioData.age);

// // Convert JS Object to JSON
// const jsonData = JSON.stringify(bioData);
// console.log(jsonData);

// // Convert JSON to JS Object
// const objData = JSON.parse(jsonData);
// console.log(objData);

// TASK

// 1) Convert to JSON
const jsonData = JSON.stringify(bioData);

// 2) Add it to another file
fs.writeFile("./data.json", jsonData, (err) => {
  console.log("File added successfully");
});

// 3) Read that file
fs.readFile("./data.json", "utf-8", (err, data) => {
  // 4) Convert back to JS Object
  if (data) {
    const objData = JSON.parse(data);
    console.log(objData);
  } else {
    console.log(err.message);
  }
});
