const add = (num1, num2) => {
  return num1 + num2;
};

const sub = (num1, num2) => {
  return num1 - num2;
};

const mult = (num1, num2) => {
  return num1 * num2;
};

// module.exports.add = add;
// module.exports.sub = sub;
// module.exports.mult = mult;
module.exports = { add, sub, mult };
