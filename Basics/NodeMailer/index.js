const express = require("express");

const app = express();
const sendMail = require("./controllers/sendMail");
let PORT = 8000;

app.get("/mail", sendMail);

const start = async () => {
  try {
    app.listen(PORT, () => {
      console.log(`Listening the server at port no: ${PORT}`);
    });
  } catch (error) {}
};

start();
