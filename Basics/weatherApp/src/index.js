const http = require("http");
const fs = require("fs");
const requests = require("requests");
let port = 8000;

const homeFile = fs.readFileSync("home.html", "utf-8");

const replaceVal = (templateVal, orgVal) => {
  let temperature = templateVal.replace("{%tempval%}", orgVal.main.temp);
  temperature = temperature.replace("{%tempmin%}", orgVal.main.temp_min);
  temperature = temperature.replace("{%tempmax%}", orgVal.main.temp_max);
  temperature = temperature.replace("{%location%}", orgVal.name);
  temperature = temperature.replace("{%country%}", orgVal.sys.country);
  temperature = temperature.replace("{%tempstatus%}", orgVal.weather[0].main);
  return temperature;
};

const server = http.createServer((req, res) => {
  const path = req.url;
  if (path == "/") {
    requests(
      "https://api.openweathermap.org/data/2.5/weather?q=new delhi&appid=d719daf58da59b42077256a3646147be&units=metric"
    )
      .on("data", (chunk) => {
        const objData = JSON.parse(chunk);
        const arrData = [objData];
        console.log(arrData);
        const realTimeData = arrData
          .map((val) => replaceVal(homeFile, val))
          .join("");
        console.log(realTimeData);
        res.write(realTimeData);
      })
      .on("end", (err) => {
        if (err) return console.log("connection closed due to errors", err);
        res.end();
      });
  } else {
    res.end("File not found");
  }
});

server.listen(port, "127.0.0.1", (err) => {
  if (!err) {
    console.log(`Server running on port : ${port}`);
  } else {
    console.log(err);
  }
});
