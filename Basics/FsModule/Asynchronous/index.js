const fs = require("fs");

// Writing data in file
fs.writeFile("./file.txt", "Today is Awesome day!!", (err) => {
  if (err) {
    console.log("Page Not Found!!");
  } else {
    console.log("File created successfully!!");
  }
});

// Appending data Async
fs.appendFile("./file.txt", "This text is appended later.", (err) => {
  console.log("Task Completed.");
});

// Reading file Async
fs.readFile("./file.txt", "utf-8", (err, data) => {
  if (err) {
    console.log("Can't read. Something Went Wrong!!");
  } else {
    console.log(data);
  }
});
