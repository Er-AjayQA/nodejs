const fs = require("fs");

// Writing data in file
fs.writeFileSync("./file.txt", "This is node js course.");
fs.writeFileSync("./file.txt", "This is node js course by ThapaTechnical.");

// Reading data from file.
const readData = fs.readFileSync("./file.txt", "utf-8");
console.log(readData);

// Getting data in buffer dataType.
fs.writeFileSync("./bufferData.txt", "This was a buffer data.");
const bufData = fs.readFileSync("./bufferData.txt");
console.log(bufData);

// Changing the buffer data by using "toString()" method.
const data = bufData.toString();
console.log(data);

// Appending data in file.
fs.appendFileSync("./file.txt", "This text is appended");

// Rename file
fs.writeFileSync("./read.txt", "This is just a dummy file.");
fs.renameSync("./read.txt", "./renamesFile.txt");

// Delete File
fs.writeFileSync("./deleted.txt", "This file will get deleted soon.");
console.log(fs.readFileSync("./deleted.txt", "utf-8"));
fs.unlinkSync("./deleted.txt");

// Create Folder
fs.mkdirSync("../NewFolder");
fs.writeFileSync(
  "../NewFolder/file.txt",
  "New Folder was created successfuly."
);

// Delete Folder
console.log(fs.readFileSync("../NewFolder/file.txt", "utf-8"));
fs.unlinkSync("../NewFolder/file.txt");
fs.rmdirSync("../NewFolder");
