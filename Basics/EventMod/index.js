const EventEmitter = require("events");

// Creating Object of the EventEmitter Class
const event = new EventEmitter();

// Listening Event
event.on("Welcome", () => {
  console.log("Hello World!!");
});

event.on("Welcome", () => {
  console.log("Hello Ajay Kumar!!");
});

event.on("Welcome", () => {
  console.log("Hello Friends!!");
});

// Triggering Event
event.emit("Welcome");

// Events with parameters
event.on("checkPage", (sc, msg) => {
  console.log(`Status Code is ${sc} and the Status String is ${msg}`);
});

event.emit("checkPage", 200, "ok");
