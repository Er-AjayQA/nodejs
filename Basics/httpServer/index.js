const http = require("http");
const url = require("url");
const fs = require("fs");

const server = http.createServer((req, res) => {
  const path = req.url;
  console.log(path);

  const data = fs.readFileSync(`${__dirname}/UserApi/userapi.json`, "utf-8");
  const objData = JSON.parse(data);

  if (path == "/" || path == "/home") {
    res.writeHead(200, { "content-type": "Home-Page" });
    res.end("This is Home Page.");
  } else if (path == "/about") {
    res.writeHead(200, { "content-type": "About-Page" });
    res.end("This is About Page.");
  } else if (path == "/userapi") {
    console.log(objData[4].firstName);
    res.writeHead(200, { "content-type": "application/json" });
    res.end(data);
  } else {
    res.writeHead(404, { "content-type": "text/html" });
    res.end(`<h2>Page not found!</h2>`);
  }
});

server.listen(8000, "127.0.0.1", () => {
  console.log("Listening to the server at port number: 8000");
});
